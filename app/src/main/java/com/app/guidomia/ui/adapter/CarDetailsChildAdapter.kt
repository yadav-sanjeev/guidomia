package com.app.guidomia.ui.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.app.guidomia.databinding.ItemProsConsLayoutBinding

/*
* @author sanjeev.kumar
* */
class CarDetailsChildAdapter(private val itemList: ArrayList<String>?) :
    RecyclerView.Adapter<CarDetailsChildAdapter.ViewHolder>() {

    inner class ViewHolder(val binding: ItemProsConsLayoutBinding) : RecyclerView.ViewHolder(binding.root) {
        fun bind(position: Int){
            binding.tvProsCons.text = "${itemList?.get(position)}"
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val binding = ItemProsConsLayoutBinding.inflate(LayoutInflater.from(parent.context),parent,false)
        return ViewHolder(binding)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(position)
    }

    override fun getItemCount(): Int {
        return itemList?.size?:0
    }
}