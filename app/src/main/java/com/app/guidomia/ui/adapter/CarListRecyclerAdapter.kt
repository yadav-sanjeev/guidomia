package com.app.guidomia.ui.adapter

import android.annotation.SuppressLint
import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import androidx.recyclerview.widget.RecyclerView
import com.app.guidomia.R
import com.app.guidomia.ui.model.CarModel
import com.app.guidomia.databinding.ItemVehLayoutBinding
import com.app.guidomia.utils.getCount
import com.app.guidomia.utils.CAR_TYPE_ALPINE
import com.app.guidomia.utils.CAR_TYPE_BAW
import com.app.guidomia.utils.CAR_TYPE_LAND_ROVER
import com.bumptech.glide.Glide

/*
* @author sanjeev.kumar
* */
class CarListRecyclerAdapter(private val context: Context) :
    RecyclerView.Adapter<CarListRecyclerAdapter.ViewHolder>() {

    var carList = ArrayList<CarModel>()

    inner class ViewHolder(val binding: ItemVehLayoutBinding) :
        RecyclerView.ViewHolder(binding.root) {
            @SuppressLint("SetTextI18n")
            fun bind(position: Int){
                val carDataModel = carList.get(position)
                binding.tvMakeModel.text = "${carDataModel.make} ${carDataModel.model}"
                binding.tvUserPrice.text =
                    "${context.getString(R.string.car_price_title)} ${(carDataModel.customerPrice as? Number)?.let { getCount(it) }}"
                binding.ratingBar.rating = carDataModel.rating
                loadImages(carDataModel.make, binding.ivVehicle, context)
                binding.gpExpandable.visibility = if (carList.get(position).expandable) View.VISIBLE else View.GONE
                setProsConsAdapter(carDataModel.prosList, binding.rvPros)
                setProsConsAdapter(carDataModel.consList, binding.rvCons)

                binding.mainLayout.setOnClickListener {
                    expandCollapseList(carDataModel)
                }
            }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val binding =
            ItemVehLayoutBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return ViewHolder(binding)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(position)
    }

    override fun getItemCount(): Int {
        return carList.size
    }

    private fun setProsConsAdapter(prosConsList: ArrayList<String>?, recyclerView: RecyclerView) {
        val adapter = CarDetailsChildAdapter(prosConsList)
        recyclerView.adapter = adapter
    }

    @SuppressLint("NotifyDataSetChanged")
    fun updateCarList(carList: ArrayList<CarModel>) {
        this.carList = carList
        this.carList.forEach { it.expandable = false }
        this.carList.firstNotNullOfOrNull {it.expandable = true }
        notifyDataSetChanged()
    }

    private fun loadImages(carType: String, imageView: ImageView, context: Context) {
        when (carType) {
            CAR_TYPE_LAND_ROVER -> loadImageUsingGlide(
                R.drawable.range_rover,
                imageView = imageView,
                context = context
            )
            CAR_TYPE_ALPINE -> loadImageUsingGlide(
                R.drawable.alpine_roadster,
                imageView = imageView,
                context = context
            )
            CAR_TYPE_BAW -> loadImageUsingGlide(
                R.drawable.bmw_330i,
                imageView = imageView,
                context = context
            )
            else -> loadImageUsingGlide(
                R.drawable.mercedez_benz_glc,
                imageView = imageView,
                context = context
            )
        }

    }

    private fun loadImageUsingGlide(resId: Int, imageView: ImageView, context: Context) {
        Glide.with(context).load(resId).into(imageView)
    }

    @SuppressLint("NotifyDataSetChanged")
    private fun expandCollapseList(carDataModel : CarModel){
        for (data in carList) {
            if (data.make == carDataModel.make && !(carDataModel.expandable))
                carList.find { it.make == data.make }?.expandable = true
            else if (data.make == carDataModel.make && (carDataModel.expandable ))
                carList.find { it.make == data.make }?.expandable = false
            else
                carList.find { it.make == data.make}?.expandable = false
        }
        notifyDataSetChanged()
    }
}