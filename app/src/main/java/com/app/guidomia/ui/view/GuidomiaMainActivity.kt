package com.app.guidomia.ui.view

import android.content.Context
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.Spinner
import androidx.lifecycle.ViewModelProvider
import com.app.guidomia.R
import com.app.guidomia.databinding.ActivityGuidomiaMainBinding
import com.app.guidomia.ui.adapter.CarListRecyclerAdapter
import com.app.guidomia.ui.model.CarModel
import com.app.guidomia.ui.viewmodel.GuidomiaViewModel
import com.app.guidomia.utils.CAR_TYPE_MAKE
import com.app.guidomia.utils.CAR_TYPE_MODEL

/*
* @author sanjeev.kumar
* */
class GuidomiaMainActivity : AppCompatActivity(), AdapterView.OnItemClickListener {

    private var carList: ArrayList<CarModel> = ArrayList()
    private var carMakeList: ArrayList<String> = ArrayList()
    private var carModelList: ArrayList<String> = ArrayList()
    private lateinit var carAdapter: CarListRecyclerAdapter
    private lateinit var carMakeAdapter: ArrayAdapter<String>
    private lateinit var carModelAdapter: ArrayAdapter<String>

    private val activityGuidomiaMainBinding by lazy {
        ActivityGuidomiaMainBinding.inflate(layoutInflater)
    }

    private val roomDBViewModel by lazy { ViewModelProvider(this)[GuidomiaViewModel::class.java] }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(activityGuidomiaMainBinding.root)
        init()
    }

    private fun init() {
        carAdapter = CarListRecyclerAdapter(this)
        carMakeList.add(getString(R.string.car_any_make))
        carModelList.add(getString(R.string.car_any_model))
        setUpDropDownAdapter(
            CAR_TYPE_MAKE,
            activityGuidomiaMainBinding.spinnerCarMake,
            this
        )
        setUpDropDownAdapter(
            CAR_TYPE_MODEL,
            activityGuidomiaMainBinding.spinnerCarModel,
            this
        )
        activityGuidomiaMainBinding.recyclerViewCarList.adapter = carAdapter
        setDropDownListener()
        setObservers()
        roomDBViewModel.fetchCarList(this)
    }

    // observer for car list, getting car list from db and update rv
    private fun setObservers() {
        roomDBViewModel.carListLiveData.observe(this) {
            it?.let { carList.addAll(it) }
            carAdapter.updateCarList(carList)
            carList.forEach { carDataModel -> carMakeList.add(carDataModel.make) }
            carList.forEach { carDataModel-> carModelList.add(carDataModel.model) }
            carMakeAdapter.notifyDataSetChanged()
            carModelAdapter.notifyDataSetChanged()
        }
    }

    // setdropdown adapter for car make and car model filters
    private fun setUpDropDownAdapter(
        type: Int,
        view: Spinner, context: Context
    ) {
        when (type) {
            CAR_TYPE_MAKE -> {
                carMakeAdapter = ArrayAdapter(
                    context,
                    androidx.appcompat.R.layout.support_simple_spinner_dropdown_item,
                    carMakeList
                )
                view.adapter = carMakeAdapter
            }
            CAR_TYPE_MODEL -> {
                carModelAdapter = ArrayAdapter(
                    context,
                    androidx.appcompat.R.layout.support_simple_spinner_dropdown_item,
                    carModelList
                )
                view.adapter = carModelAdapter
            }
        }
    }

    // set dropdown listener for car model and car make
    private fun setDropDownListener() {

        activityGuidomiaMainBinding.spinnerCarMake.onItemSelectedListener =
            object : AdapterView.OnItemSelectedListener {
                override fun onNothingSelected(parent: AdapterView<*>?) {

                }

                override fun onItemSelected(
                    parent: AdapterView<*>?,
                    view: View?,
                    position: Int,
                    id: Long
                ) {
                    if (position > 0) {
                        carAdapter.updateCarList(carList.filter { it.make == (carList[position - 1].make) } as ArrayList<CarModel>)
                    } else
                        carAdapter.updateCarList(carList)
                }

            }

        activityGuidomiaMainBinding.spinnerCarModel.onItemSelectedListener =
            object : AdapterView.OnItemSelectedListener {
                override fun onNothingSelected(parent: AdapterView<*>?) {

                }

                override fun onItemSelected(
                    parent: AdapterView<*>?,
                    view: View?,
                    position: Int,
                    id: Long
                ) {
                    if (position > 0) {
                        carAdapter.updateCarList(carList.filter { it.model == (carList[position - 1].model) } as ArrayList<CarModel>)
                    } else
                        carAdapter.updateCarList(carList)
                }

            }
    }

    override fun onItemClick(p0: AdapterView<*>?, p1: View?, p2: Int, p3: Long) {

    }
}