package com.app.guidomia.ui.viewmodel

import android.content.Context
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.app.guidomia.ui.model.CarModel
import com.app.guidomia.db.dao.CarDao
import com.app.guidomia.db.dbHelper.GuidomiaDBBuilder
import com.app.guidomia.utils.getCarDataFromJson
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

/*
* @author sanjeev.kumar
* */
class GuidomiaViewModel : ViewModel() {

    private val _carListLiveData = MutableLiveData<ArrayList<CarModel>>()
    val carListLiveData: LiveData<ArrayList<CarModel>> = _carListLiveData

    // fetching carlist from db and json
    fun fetchCarList(context: Context) {
        viewModelScope.launch {
            val result = withContext(Dispatchers.IO) {
                (getDBObject(context).getAllCarList() as ArrayList<CarModel>)
            }
            if (result.isEmpty())
                getCarListFromJson(context)
            else
                _carListLiveData.value = result
        }
    }

    // insert carlist in db
    private fun insertCarList(carList: ArrayList<CarModel>, context: Context) {
        viewModelScope.launch {
            launch { getDBObject(context).insertAllCarList(carList) }
        }
    }

    // get carlist from json
    private fun getCarListFromJson(context: Context) {
        viewModelScope.launch {
            val carListFromJson = withContext(Dispatchers.IO) { context.getCarDataFromJson() }
            _carListLiveData.value = carListFromJson
            insertCarList(carListFromJson, context)
        }
    }

    // get db instance
    private fun getDBObject(context: Context): CarDao {
        return GuidomiaDBBuilder.getInstance(context).CarDao()
    }
}