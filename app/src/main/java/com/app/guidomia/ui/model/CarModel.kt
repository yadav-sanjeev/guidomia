package com.app.guidomia.ui.model

import androidx.room.Entity
import androidx.room.PrimaryKey

/*
* @author sanjeev.kumar
* */
@Entity(tableName = "car")
data class CarModel(
    @PrimaryKey(autoGenerate = true)
    var id: Int = 0,
    var consList: ArrayList<String>? = null,
    var customerPrice: Float = 0.0f,
    var make: String = "",
    var marketPrice: Float = 0.0f,
    var model: String = "",
    var prosList: ArrayList<String>? = null,
    var rating: Float = 0.0f,
    var expandable: Boolean = false
)
