package com.app.guidomia.utils

import android.content.Context
import com.app.guidomia.ui.model.CarModel
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import java.io.IOException
import java.text.DecimalFormat

/*
* @author sanjeev.kumar
* */

// get carlist data from json
fun Context.getCarDataFromJson(): ArrayList<CarModel> {
    var jsonString = ""
    try {
        jsonString =
            this.assets.open(ASSET_CAR_LIST_FILE_NAME).bufferedReader().use { it.readText() }
    } catch (ioException: IOException) {
        ioException.printStackTrace()
        return ArrayList()
    }
    val listPersonType = object : TypeToken<List<CarModel>>() {}.type
    return Gson().fromJson(jsonString, listPersonType) as ArrayList<CarModel>
}

// get number in thousand = k, million  = m
fun getCount(number: Number): String? {
    val suffix = charArrayOf(' ', 'k', 'M', 'B', 'T', 'P', 'E')
    val numValue = number.toLong()
    val value = Math.floor(Math.log10(numValue.toDouble())).toInt()
    val base = value / 3
    return if (value >= 3 && base < suffix.size) {
        DecimalFormat("#0").format(
            numValue / Math.pow(
                10.0,
                (base * 3).toDouble()
            )
        ) + suffix[base]
    } else {
        DecimalFormat("#,##0").format(numValue)
    }
}
