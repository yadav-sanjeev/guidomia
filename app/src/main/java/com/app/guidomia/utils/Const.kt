package com.app.guidomia.utils

const val ASSET_CAR_LIST_FILE_NAME = "car_list.json"
const val CAR_TYPE_LAND_ROVER = "Land Rover"
const val CAR_TYPE_ALPINE = "Alpine"
const val CAR_TYPE_BAW = "BMW"
const val CAR_TYPE_MAKE = 1
const val CAR_TYPE_MODEL = 2