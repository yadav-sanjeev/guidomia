package com.app.guidomia.db.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query
import com.app.guidomia.ui.model.CarModel

/*
* @author sanjeev.kumar
* CRED operations
* */
@Dao
interface CarDao {

    @Query("SELECT * FROM car")
    suspend fun getAllCarList() : List<CarModel>

    @Insert
    suspend fun insertAllCarList(carList: ArrayList<CarModel>)
}