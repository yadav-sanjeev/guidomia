package com.app.guidomia.db.dbHelper

import android.content.Context
import androidx.room.Room
import com.app.guidomia.db.GuidomiaAppDB

/*
* @author sanjeev.kumar
* DB instance builder
* */
object GuidomiaDBBuilder {

    private var INSTANCE: GuidomiaAppDB? = null
    private val DB_NAME = "guidomia_db"

    fun getInstance(context: Context): GuidomiaAppDB {
        if (INSTANCE == null) {
            synchronized(GuidomiaAppDB::class) {
                INSTANCE = buildRoomDB(context)
            }
        }
        return INSTANCE!!
    }

    private fun buildRoomDB(context: Context) =
        Room.databaseBuilder(
            context.applicationContext,
            GuidomiaAppDB::class.java,
            DB_NAME
        ).build()
}