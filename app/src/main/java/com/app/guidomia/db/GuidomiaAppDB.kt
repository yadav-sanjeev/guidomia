package com.app.guidomia.db

import androidx.room.Database
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import com.app.guidomia.db.dao.CarDao
import com.app.guidomia.db.dbHelper.DBModelConverters
import com.app.guidomia.ui.model.CarModel

/*
* @author sanjeev.kumar
* */
@Database(entities = [CarModel::class], version = 1)
@TypeConverters(DBModelConverters::class)
abstract class GuidomiaAppDB : RoomDatabase() {
    abstract fun CarDao(): CarDao
}